%{
#include <stdio.h>
#include <string.h>
int yylex(void);
void yyerror(char *);
%}

%union{
    int inum;
    double dnum;
    char ch;
}
%token <dnum> NUM
%type <dnum> E T F

%token ADD SUB MUL DIV CR LEFT RIGHT
%start linelist
%%
linelist: line
    | linelist line
line : E CR  {printf("识别成功表达式的值为 %lf\n\n",$1);}
E: T
    | E ADD T {printf("规约 E -> E + T\n"); $$=$1+$3;}
    | E SUB T {printf("规约 E -> E - T\n"); $$=$1-$3;}
    ;
T: F
    | T MUL F {printf("规约 T -> T * num\n"); $$=$1*$3;}
    | T DIV F {printf("规约 T -> T / num\n"); $$=$1/$3;}
    ;

F: LEFT E RIGHT {printf("规约 F -> (E)\n"); $$=$2;}
    | NUM {printf("规约 F -> num\n"); $$=$1;}

%%
void yyerror(char *str){
    fprintf(stderr,"error:%s\n",str);
}
int yywrap(){
    return 1;
}
int main()
{
    yyparse();
}