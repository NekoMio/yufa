LR1 文法分析表: 
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
|    | +   | -   | *   | /   | (  | )   | num | $   |  | E  | T  | F  | E'' |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 0  |     |     |     |     | S1 |     | S4  |     |  | 2  | 5  | 3  |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 1  |     |     |     |     | S9 |     | S10 |     |  | 6  | 7  | 8  |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 2  | S12 | S11 |     |     |    |     |     | ACC |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 3  | R8  | R8  | R8  | R8  |    |     |     | R8  |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 4  | R5  | R5  | R5  | R5  |    |     |     | R5  |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 5  | R2  | R2  | S13 | S14 |    |     |     | R2  |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 6  | S15 | S16 |     |     |    | S17 |     |     |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 7  | R2  | R2  | S18 | S19 |    | R2  |     |     |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 8  | R8  | R8  | R8  | R8  |    | R8  |     |     |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 9  |     |     |     |     | S9 |     | S10 |     |  | 20 | 7  | 8  |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 10 | R5  | R5  | R5  | R5  |    | R5  |     |     |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 11 |     |     |     |     | S1 |     | S4  |     |  |    | 21 | 3  |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 12 |     |     |     |     | S1 |     | S4  |     |  |    | 22 | 3  |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 13 |     |     |     |     | S1 |     | S4  |     |  |    |    | 23 |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 14 |     |     |     |     | S1 |     | S4  |     |  |    |    | 24 |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 15 |     |     |     |     | S9 |     | S10 |     |  |    | 25 | 8  |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 16 |     |     |     |     | S9 |     | S10 |     |  |    | 26 | 8  |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 17 | R4  | R4  | R4  | R4  |    |     |     | R4  |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 18 |     |     |     |     | S9 |     | S10 |     |  |    |    | 27 |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 19 |     |     |     |     | S9 |     | S10 |     |  |    |    | 28 |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 20 | S15 | S16 |     |     |    | S29 |     |     |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 21 | R1  | R1  | S13 | S14 |    |     |     | R1  |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 22 | R0  | R0  | S13 | S14 |    |     |     | R0  |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 23 | R7  | R7  | R7  | R7  |    |     |     | R7  |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 24 | R6  | R6  | R6  | R6  |    |     |     | R6  |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 25 | R0  | R0  | S18 | S19 |    | R0  |     |     |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 26 | R1  | R1  | S18 | S19 |    | R1  |     |     |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 27 | R7  | R7  | R7  | R7  |    | R7  |     |     |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 28 | R6  | R6  | R6  | R6  |    | R6  |     |     |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
| 29 | R4  | R4  | R4  | R4  |    | R4  |     |     |  |    |    |    |     |
+----+-----+-----+-----+-----+----+-----+-----+-----+--+----+----+----+-----+
LR1 分析过程为: 
+------+---------------------------------------------------------+----------------------------------+------------+
| 步骤 | 栈                                                      | 输入                             | 动作       |
+======+=========================================================+==================================+============+
| 0    |  0 | 4                                                  | num * (num + num / (num - num))$ | 移进4      |
|      | ---+-----                                               |                                  |            |
|      |  $ | num                                                |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 1    |  0 | 3                                                  | * (num + num / (num - num))$     | 规约F->num |
|      | ---+---                                                 |                                  |            |
|      |  $ | F                                                  |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 2    |  0 | 5                                                  | * (num + num / (num - num))$     | 规约T->F   |
|      | ---+---                                                 |                                  |            |
|      |  $ | T                                                  |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 3    |  0 | 5 | 13                                             | * (num + num / (num - num))$     | 移进13     |
|      | ---+---+----                                            |                                  |            |
|      |  $ | T | *                                              |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 4    |  0 | 5 | 13 | 1                                         | (num + num / (num - num))$       | 移进1      |
|      | ---+---+----+---                                        |                                  |            |
|      |  $ | T | *  | (                                         |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 5    |  0 | 5 | 13 | 1 | 10                                    | num + num / (num - num))$        | 移进10     |
|      | ---+---+----+---+-----                                  |                                  |            |
|      |  $ | T | *  | ( | num                                   |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 6    |  0 | 5 | 13 | 1 | 8                                     | + num / (num - num))$            | 规约F->num |
|      | ---+---+----+---+---                                    |                                  |            |
|      |  $ | T | *  | ( | F                                     |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 7    |  0 | 5 | 13 | 1 | 7                                     | + num / (num - num))$            | 规约T->F   |
|      | ---+---+----+---+---                                    |                                  |            |
|      |  $ | T | *  | ( | T                                     |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 8    |  0 | 5 | 13 | 1 | 6                                     | + num / (num - num))$            | 规约E->T   |
|      | ---+---+----+---+---                                    |                                  |            |
|      |  $ | T | *  | ( | E                                     |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 9    |  0 | 5 | 13 | 1 | 6 | 15                                | + num / (num - num))$            | 移进15     |
|      | ---+---+----+---+---+----                               |                                  |            |
|      |  $ | T | *  | ( | E | +                                 |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 10   |  0 | 5 | 13 | 1 | 6 | 15 | 10                           | num / (num - num))$              | 移进10     |
|      | ---+---+----+---+---+----+-----                         |                                  |            |
|      |  $ | T | *  | ( | E | +  | num                          |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 11   |  0 | 5 | 13 | 1 | 6 | 15 | 8                            | / (num - num))$                  | 规约F->num |
|      | ---+---+----+---+---+----+---                           |                                  |            |
|      |  $ | T | *  | ( | E | +  | F                            |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 12   |  0 | 5 | 13 | 1 | 6 | 15 | 25                           | / (num - num))$                  | 规约T->F   |
|      | ---+---+----+---+---+----+----                          |                                  |            |
|      |  $ | T | *  | ( | E | +  | T                            |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 13   |  0 | 5 | 13 | 1 | 6 | 15 | 25 | 19                      | / (num - num))$                  | 移进19     |
|      | ---+---+----+---+---+----+----+----                     |                                  |            |
|      |  $ | T | *  | ( | E | +  | T  | /                       |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 14   |  0 | 5 | 13 | 1 | 6 | 15 | 25 | 19 | 9                  | (num - num))$                    | 移进9      |
|      | ---+---+----+---+---+----+----+----+---                 |                                  |            |
|      |  $ | T | *  | ( | E | +  | T  | /  | (                  |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 15   |  0 | 5 | 13 | 1 | 6 | 15 | 25 | 19 | 9 | 10             | num - num))$                     | 移进10     |
|      | ---+---+----+---+---+----+----+----+---+-----           |                                  |            |
|      |  $ | T | *  | ( | E | +  | T  | /  | ( | num            |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 16   |  0 | 5 | 13 | 1 | 6 | 15 | 25 | 19 | 9 | 8              | - num))$                         | 规约F->num |
|      | ---+---+----+---+---+----+----+----+---+---             |                                  |            |
|      |  $ | T | *  | ( | E | +  | T  | /  | ( | F              |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 17   |  0 | 5 | 13 | 1 | 6 | 15 | 25 | 19 | 9 | 7              | - num))$                         | 规约T->F   |
|      | ---+---+----+---+---+----+----+----+---+---             |                                  |            |
|      |  $ | T | *  | ( | E | +  | T  | /  | ( | T              |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 18   |  0 | 5 | 13 | 1 | 6 | 15 | 25 | 19 | 9 | 20             | - num))$                         | 规约E->T   |
|      | ---+---+----+---+---+----+----+----+---+----            |                                  |            |
|      |  $ | T | *  | ( | E | +  | T  | /  | ( | E              |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 19   |  0 | 5 | 13 | 1 | 6 | 15 | 25 | 19 | 9 | 20 | 16        | - num))$                         | 移进16     |
|      | ---+---+----+---+---+----+----+----+---+----+----       |                                  |            |
|      |  $ | T | *  | ( | E | +  | T  | /  | ( | E  | -         |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 20   |  0 | 5 | 13 | 1 | 6 | 15 | 25 | 19 | 9 | 20 | 16 | 10   | num))$                           | 移进10     |
|      | ---+---+----+---+---+----+----+----+---+----+----+----- |                                  |            |
|      |  $ | T | *  | ( | E | +  | T  | /  | ( | E  | -  | num  |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 21   |  0 | 5 | 13 | 1 | 6 | 15 | 25 | 19 | 9 | 20 | 16 | 8    | ))$                              | 规约F->num |
|      | ---+---+----+---+---+----+----+----+---+----+----+---   |                                  |            |
|      |  $ | T | *  | ( | E | +  | T  | /  | ( | E  | -  | F    |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 22   |  0 | 5 | 13 | 1 | 6 | 15 | 25 | 19 | 9 | 20 | 16 | 26   | ))$                              | 规约T->F   |
|      | ---+---+----+---+---+----+----+----+---+----+----+----  |                                  |            |
|      |  $ | T | *  | ( | E | +  | T  | /  | ( | E  | -  | T    |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 23   |  0 | 5 | 13 | 1 | 6 | 15 | 25 | 19 | 9 | 20             | ))$                              | 规约E->E-T |
|      | ---+---+----+---+---+----+----+----+---+----            |                                  |            |
|      |  $ | T | *  | ( | E | +  | T  | /  | ( | E              |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 24   |  0 | 5 | 13 | 1 | 6 | 15 | 25 | 19 | 9 | 20 | 29        | ))$                              | 移进29     |
|      | ---+---+----+---+---+----+----+----+---+----+----       |                                  |            |
|      |  $ | T | *  | ( | E | +  | T  | /  | ( | E  | )         |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 25   |  0 | 5 | 13 | 1 | 6 | 15 | 25 | 19 | 28                 | )$                               | 规约F->(E) |
|      | ---+---+----+---+---+----+----+----+----                |                                  |            |
|      |  $ | T | *  | ( | E | +  | T  | /  | F                  |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 26   |  0 | 5 | 13 | 1 | 6 | 15 | 25                           | )$                               | 规约T->T/F |
|      | ---+---+----+---+---+----+----                          |                                  |            |
|      |  $ | T | *  | ( | E | +  | T                            |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 27   |  0 | 5 | 13 | 1 | 6                                     | )$                               | 规约E->E+T |
|      | ---+---+----+---+---                                    |                                  |            |
|      |  $ | T | *  | ( | E                                     |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 28   |  0 | 5 | 13 | 1 | 6 | 17                                | )$                               | 移进17     |
|      | ---+---+----+---+---+----                               |                                  |            |
|      |  $ | T | *  | ( | E | )                                 |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 29   |  0 | 5 | 13 | 23                                        | $                                | 规约F->(E) |
|      | ---+---+----+----                                       |                                  |            |
|      |  $ | T | *  | F                                         |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 30   |  0 | 5                                                  | $                                | 规约T->T*F |
|      | ---+---                                                 |                                  |            |
|      |  $ | T                                                  |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 31   |  0 | 2                                                  | $                                | 规约E->T   |
|      | ---+---                                                 |                                  |            |
|      |  $ | E                                                  |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
| 32   |  0                                                      | $                                | 接受       |
|      | ---                                                     |                                  |            |
|      |  $                                                      |                                  |            |
+------+---------------------------------------------------------+----------------------------------+------------+
